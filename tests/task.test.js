const request = require('supertest');
const app = require('../src/app');
const Task = require('../src/models/task');
const {
    userOne,
    userOneId,
    userTwo,
    userTwoId,
    taskOne,
    taskTwo,
    setupDatabase
} = require('./fixtures/db');

// //
// // Task Test Ideas
// //
// +	Should not create task with invalid completed
// +	Should not create task with invalid description
// +	Should not update task with invalid description/completed
// +	Should delete user task
// +	Should not delete task if unauthenticated
// + 	Should not update other users task
// +	Should fetch user task by id
// +	Should not fetch user task by id if unauthenticated
// +	Should not fetch other users task by id
// +	Should fetch only completed tasks
// + 	Should fetch only incomplete tasks
// +	Should sort tasks by description/completed/createdAt/updatedAt
// +	Should fetch page of tasks


beforeEach(setupDatabase);

test('Should create task for user', async () => {
    const response = await request(app)
        .post('/tasks')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            description: 'From my test'
        })
        .expect(201)
    const task = await Task.findById(response.body._id);
    expect(task).not.toBeNull();
    expect(task.description).toBe(response.body.description);
    expect(task.completed).toBe(false);
})

test('Should not create task with invalid completed', async () => {
    const response = await request(app)
        .post('/tasks')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            description: 'Some desc',
            completed: { ff: 'fff' }
        })
        .expect(400);
    const task = await Task.findById(response.body._id);
    expect(task).toBeNull();
})

test('Should not create task with invalid description', async () => {
    const response = await request(app)
        .post('/tasks')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            description: undefined
        })
        .expect(400);
    const task = await Task.findById(response.body._id);
    expect(task).toBeNull();
})

test('Should not update task with invalid description', async () => {
    const response = await request(app)
        .patch(`/tasks/${taskOne._id}`)
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            description: null
        })
        .expect(400);
    const task = await Task.findById(response.body._id);
    expect(task).toBeNull();
})

test('Should not update task with invalid completed', async () => {
    const response = await request(app)
        .patch(`/tasks/${taskOne._id}`)
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            completed: { ff: 'fff' }
        })
        .expect(400);
    const task = await Task.findById(response.body._id);
    expect(task).toBeNull();
})

test('Should return all tasks for userOne', async () => {
    const response = await request(app)
        .get('/tasks')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
    expect(response.body.length).toEqual(2);
})


test('Should not delete tasks of a user1 with user2', async () => {
    const response = await request(app)
        .delete(`/tasks/${taskOne._id}`)
        .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
        .send()
        .expect(404);
    const task = await Task.findById(taskOne._id);
    expect(task).not.toBeNull();
})

test('Should delete task', async () => {
    const response = await request(app)
        .delete(`/tasks/${taskOne._id}`)
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
    const task = await Task.findById(taskOne._id);
    expect(task).toBeNull();
})

test('Should not delete task for unautenticated user', async () => {
    const response = await request(app)
        .delete(`/tasks/${taskOne._id}`)
        .send()
        .expect(401)
    const task = await Task.findById(taskOne._id);
    expect(task).not.toBeNull();
})

test('Should not update other users task', async () => {
    await request(app)
        .patch(`/tasks/${taskOne._id}`)
        .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
        .send()
        .expect(404)
})

test('Should fetch user task by id', async () => {
    const response = await request(app)
        .get(`/tasks/${taskOne._id}`)
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
    const task = await Task.findById(response.body._id);
    expect(task).not.toBeNull();
    expect(response.body.description).toBe(task.description);
    expect(response.body.completed).toBe(task.completed);
})

test('Should not fetch user task by id if unauthenticated', async () => {
    await request(app)
        .get(`/tasks/${taskOne._id}`)
        .send()
        .expect(401)
})

test('Should not fetch other users task by id', async () => {
    await request(app)
        .get(`/tasks/${taskOne._id}`)
        .set('Authorization', `Bearer ${userTwo.tokens[0].token}`)
        .send()
        .expect(404)
})

test('Should fetch only completed tasks', async () => {
    const response = await request(app)
        .get('/tasks?completed=true')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
    expect(response.body.length).toBe(1);
    expect(response.body[0].completed).toBe(true);
})

test('Should fetch only incomplete tasks', async () => {
    const response = await request(app)
        .get('/tasks?completed=false')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
    expect(response.body.length).toBe(1);
    expect(response.body[0].completed).toBe(false);
})

test('Should sort tasks by createdAt', async () => {
    const response = await request(app)
        .get('/tasks?sortBy=createdAt:asc')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
    expect(response.body.length).toBe(2);
    expect(response.body[0].description).toEqual(taskOne.description);
    expect(response.body[1].description).toEqual(taskTwo.description);
})

test('Should sort tasks by description', async () => {
    const response = await request(app)
        .get('/tasks?sortBy=description:desc')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
    expect(response.body.length).toBe(2);
    expect(response.body[0].description).toEqual(taskTwo.description);
    expect(response.body[1].description).toEqual(taskOne.description);
})

test('Should sort tasks by completed', async () => {
    const response = await request(app)
        .get('/tasks?sortBy=completed:desc')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
    expect(response.body.length).toBe(2);
    expect(response.body[0].description).toEqual(taskTwo.description);
    expect(response.body[1].description).toEqual(taskOne.description);
})


test('Should sort tasks by updatedAt', async () => {
    const response = await request(app)
        .get('/tasks?sortBy=updatedAt:asc')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
    expect(response.body.length).toBe(2);
    expect(response.body[0].description).toEqual(taskOne.description);
    expect(response.body[1].description).toEqual(taskTwo.description);
})

// test('Should fetch page of tasks', async () => {
//     const response = await request(app)
//         .get('/tasks?sortBy=createdAt:desc&limit=1')
//         .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
//         .send()
//         .expect(200)
//     expect(response.body.length).toBe(1);
//     expect(response.body[0].description).toEqual(taskTwo.description);
// })