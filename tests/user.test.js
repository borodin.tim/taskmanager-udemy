const request = require('supertest');
const app = require('../src/app');
const User = require('../src/models/user');
const { userOne, userOneId, setupDatabase } = require('./fixtures/db');

beforeEach(setupDatabase);

// //
// // User Test Ideas
// //
// +	Should not signup user with invalid name/email/password
// +	Should not update user if unauthenticated
// +	Should not update user with invalid name/email/password
// +	Should not delete user if unauthenticated

test('Should sign up a new user', async () => {
    const response = await request(app)
        .post('/users')
        .send({
            name: 'Timur',
            email: 'timur@gmail.com',
            password: 'TestPW123!'
        })
        .expect(201)

    // Assert that the DB was changed correctly
    const user = await User.findById(response.body.user._id);
    expect(user).not.toBeNull();

    // Assertions about the response
    // expect(response.body.user.name).toBe('Marina');
    expect(response.body).toMatchObject({
        user: {
            name: 'Timur',
            email: 'timur@gmail.com',
        },
        token: user.tokens[0].token
    });

    expect(user.password).not.toBe('TestPW123!');
})

test('Should Log in existing user', async () => {
    const response = await request(app).
        post('/users/login')
        .send({
            email: userOne.email,
            password: userOne.password
        })
        .expect(200)

    const user = await User.findById(userOneId);
    expect(user).not.toBeNull();

    expect(response.body.token).toBe(user.tokens[1].token);
})

test('Should not log in non-existing user', async () => {
    await request(app)
        .post('/user/login')
        .send({
            email: 'nonExistingEmail@gmail.com',
            password: 'incorrectPW'
        })
        .expect(404);
})

test('Should get profile for user', async () => {
    await request(app)
        .get('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)
})

test('Shouold not get profile for unauthenticated user', async () => {
    await request(app)
        .get('/users/me')
        .send()
        .expect(401)
})

test('Should delete account for authenticated user', async () => {
    await request(app)
        .delete('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send()
        .expect(200)

    const user = await User.findById(userOneId);
    expect(user).toBeNull();
})

test('Should not delete user for unauthenticated user', async () => {
    await request(app)
        .delete('/users/me')
        .send()
        .expect(401)
})

test('Should upload avatar image', async () => {
    await request(app)
        .post('/users/me/avatar')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .attach('avatar', 'tests/fixtures/profile-pic.jpg')
        .expect(200)

    const user = await User.findById(userOneId);
    expect(user.avatar).toEqual(expect.any(Buffer));
})

test('Should update valid user fields', async () => {
    await request(app)
        .patch('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            name: 'Asdasd'
        })
        .expect(200);

    const user = await User.findById(userOneId);
    expect(user).not.toBeNull();
    expect(user.name).toBe('Asdasd');
})

test('Should update valid user fields', async () => {
    await request(app)
        .patch('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            location: 'Boston'
        })
        .expect(400);
})

test('Should not signup user with invalid name', async () => {
    const response = await request(app)
        .post('/users').send({
            name: '',
            email: 'timur@gmail.com',
            password: 'TestPW123!'
        }).expect(400)
    expect(response.body.errors).not.toBeNull();
})

test('Should not signup user with invalid email', async () => {
    const response = await request(app)
        .post('/users').send({
            name: 'Timur',
            email: 'timur@gmail',
            password: 'TestPW123!'
        }).expect(400)
    expect(response.body.errors).not.toBeNull();
})

test('Should not signup user with invalid password', async () => {
    const response = await request(app)
        .post('/users')
        .send({
            name: 'Timur',
            email: 'timur@gmail.com',
            password: '123'
        }).expect(400)
    expect(response.body.error).not.toBeNull();
})

test('Should not update user if unauthenticated', async () => {
    await request(app)
        .patch('/users/me')
        .send({
            name: 'UnauthUserUpdate'
        })
        .expect(401)
})

test('Should not update user with invalid name', async () => {
    await request(app)
        .patch('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            name: ''
        })
        .expect(400)
})

test('Should not update user with invalid email', async () => {
    await request(app)
        .patch('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            email: 'newemail@provider'
        })
        .expect(400)
})

test('Should not update user with invalid password', async () => {
    await request(app)
        .patch('/users/me')
        .set('Authorization', `Bearer ${userOne.tokens[0].token}`)
        .send({
            password: '123'
        })
        .expect(400)
})

test('Should not delete user if unauthenticated', async () => {
    await request(app)
        .delete('/users/me')
        .send()
        .expect(401)
})