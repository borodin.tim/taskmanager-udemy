// CRUD - create read update delete

// const mongodb = require('mongodb');
// const MongoClient = mongodb.MongoClient;
// const ObjectID = mongodb.ObjectID;
const { MongoClient, ObjectID } = require('mongodb');

const connectionURL = 'mongodb://127.0.0.1:27017';
const databaseName = 'task-manager';

const id = new ObjectID();
// console.log(id.id.length);7
// console.log(id.getTimestamp());

MongoClient.connect(connectionURL, { useNewUrlParser: true, useUnifiedTopology: true }, (error, client) => {
    //  this callback operation  will run when the connection finishes
    if (error) {
        return console.log('Unable to connect to database');
    }
    const db = client.db(databaseName);




});






// INSERT
   // db.collection('users').insertOne({
    //     name: 'Koko',
    //     age: '32',
    // }, (error, result) => {
    //     if (error) {
    //         return console.log('Unable to insert user');
    //     }
    //     console.log(result.ops);
    // });

    // db.collection('users').insertMany([
    //     {
    //         name: 'Harley',
    //         age: 3,
    //     }, {
    //         name: 'Martian',
    //         age: 4,
    //     }
    // ], (error, result) => {
    //         if (error) {
    //             return console.log('Unable to insert many users');
    //         }
    //         console.log(result.ops);
    // });

    // db.collection('tasks').insertMany([
    //     {
    //         description: 'Do stuff',
    //         completed: false,
    //     }, {
    //         description: 'Learn new things',
    //         completed: true,
    //     }, {
    //         description: 'Play games',
    //         completed: 'true',
    //     },
    // ], (error, result) => {
    //     if (error) {
    //         return console.log('Unable to insert tasks');
    //     }
    //     console.log(result.ops)
    // });




//FIND
        // db.collection('users').findOne({ _id: new ObjectID('5f314f42f6a8243059645cd7') }, (error, user) => {
    //     // user == document of the collection in DB2
    //     if (error) {
    //         return console.log('Enable to find one');
    //     }
    //     //console.log(user);
    // });

    // db.collection('users').find({ age: 32 }).toArray((error, users) => {
    //     console.log(users);
    // });

    // db.collection('users').find({ age: 32 }).count((error, count) => {
    //     console.log(count);
    // });


    // db.collection('tasks').findOne({_id: new ObjectID('5f31978cb84f56b08ed67555')}, (error, task) => {
    //     console.log(task);
    // });

    // db.collection('tasks').find({ completed: false }).toArray((error, ongoingTasks) => { 
    //     console.log(ongoingTasks);
    // });




// UPDATE
 // db.collection('users').updateOne({
    //     _id: new ObjectID('5f314f42f6a8243059645cd7')
    // }, {
    //     // $set: {
    //     //     name: 'Moshe'
    //     // }
    //     $inc: {
    //         age: 2
    //     }
    // }).then((result) => {
    //     console.log(result);
    // }).catch((error) => {
    //     console.log(error);
    // });

    // db.collection('tasks').updateMany({
    //     completed: false
    // }, {
    //     $set: {
    //         completed: true
    //     }
    // }).then((result) => {
    //     console.log(result.modifiedCount);
    // }).catch((error) => {
    //     console.log(error);
    // });




// DELETE
    // db.collection('users').deleteMany({
    //     age: { $lte: 32 }
    // }).then((result) => {
    //     console.log(result.deletedCount);
    // }).catch((error) => {
    //     console.log(error);
    // });

    // db.collection('tasks').deleteOne({
    //     description: 'Take a shower'
    // }).then((result) => {
    //     console.log(result);
    //     console.log(result.deletedCount);
    // }).catch((error) => {
    //     console.log(error);
    // });