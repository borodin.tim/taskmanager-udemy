const sgMail = require('@sendgrid/mail');

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const sendWelcomeEmail = (email, name) => {
    sgMail.send({ 
        to: email,
        from: 'borodin.tim@gmail.com',
        subject: 'Tanks for joining Task App',
        text: `Welcome to the Task App, ${name}. Let me know how you get along with the app.`
    });
}

const sendCancellationEmail = (email, name) => {
    sgMail.send({ 
        to: email,
        from: 'borodin.tim@gmail.com',
        subject: 'Your account at Task App was cancelled',
        text: `Your account at Task App was cancelled, ${name}. Please let us know if there was something we could do better.`
    });
}


module.exports = {
    sendWelcomeEmail, 
    sendCancellationEmail
};